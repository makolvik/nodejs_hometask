<<<<<<< HEAD
# NodeJS Hometask

Ryu обратился к студенту с заданием добавить возможность добавлять бойцов и менять их характеристики. Также Ryu хочет, чтобы было видно, кто конкретно пользуется приложением.

У Ryu уже есть страницы логина и регистрации, а также создание и просмотр характеристик бойцов, однако нет бэкэнд части.

## Запуск проекта
* Для первого запуска необходимо выполнить 
```
. build-start
```
* далее можно запускать с помощью команды
```
npm start
```

### Особености проекта
* В папке client находится небольшое приложение на реакте, на котором есть вьюхи регистрации, логина, добавление и выбор бойцов. Этот проект размещен для ознакомления и основная цель - показать как клиент работает с сервером. Звпросы от клиента можно посмотреть во вкладке Network в Chrome Dev tool.
* В папке config находится конфигурация базы данных. В качестве базы данных выступает файл database.json.
* В папке middlewares находятся промежуточные функции, которые отрабатывают перед контроллерами в папке routes.
* В папке repositories находятся классы для работы с базой данных для каждой сущности. При желании про паттерн репозитория можно почитать <a href="https://habr.com/ru/post/248505/" traget="_blank">здесь</a> 
* В папке routes находятся контроллеры для каждой сущности. Это точка входа для запроса в backend часть приложения.
* В папке services находятся классы для обработки запросов по бизнес правилам для каждой сущности. Очень важно, чтобы контроллеры оставались чистыми, а все бизнес равила располагались в сервисах. Это позволяет эффективнее и читабельнее писать и переиспользовать код.
* В папке models находятся модели основных сущностей приложения. Это то, в каком виде сущности хранятся в базе данных.
* index.js - это точка входа в приложение и конфигурация самого сервера.

## Задание

Необходимо реализовать REST для сущностей пользователя и бойца.
```
    USER:
        GET /api/users
        GET /api/users/:id
        POST /api/users
        PUT /api/users/:id
        DELETE /api/users/:id

    FIGHTER
        GET /api/fighters
        GET /api/fighters/:id
        POST /api/fighters
        PUT /api/fighters/:id
        DELETE /api/fighters/:id
```

Для запросов на создание и обновление сущностей необходимо реализовать валидацию через middlewares. Правила валидации определяются сущностями, в папке models. Валидировать необходимо:

* Наличие полей
* Формат полей: 
    * email - только gmail почты
    * phoneNumber: +380xxxxxxxxx
    * power - число и < 100
* Id в body запросов должен отсутствовать
* Лишние поля не должны пройти в БД

Все дополнительные валидации приветствуются.

Также необходимо реализовать middleware для выдачи ответа сервера по следующим правилам:

* Если все прошло хорошо - вернуть статус 200 и JSON
* Ошибки
    * Ошибки запроса (валидация, проблемы в обработке) - вернуть статус 400 и JSON с ошибкой
    * Если что-то не найдено - вернуть статус 404 и JSON с ошибкой

JSON ошибки формата

```
{
    error: true,
    message: ''
}
```

Постарайтесь давать лаконичные, но понятные сообщения об ошибках, например:
* User not found
* User entity to create isn't valid

При реализации домашнего задания очень важно соблюдать структуру проекта и слои:
* repositories - работа с базой
* services - бизнес логика приложения
* routes - прием запросов и отправка ответов

### Дополнительное задание
* Добавить функционал битвы из предыдущего задания
* Реализовать сохранение битвы и просмотр их историй
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
>>>>>>> e1645d297995560aa94683e4671ddcc4fc62fb85
