import { get, post } from "../requestHelper";

const entity = "fight";

export const createFight = async (body) => {
  return await post(entity, body);
};
