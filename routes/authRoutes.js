const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
      const checkUser = AuthService.login(req.body);
      res.data = checkUser;
    } catch (error) {
      const err = new Error(error);
      err.status = 400;
      next(err);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

module.exports = router;
