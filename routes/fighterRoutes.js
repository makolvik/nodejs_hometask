const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter

router.get(
  "/",
  (req, res, next) => {
    try {
      const getFighters = FighterService.getFighters();
      res.data = getFighters;
    } catch (error) {
      const err = new Error(error);
      err.status = 400;
      next(err);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);
router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    try {
      const newFighter = FighterService.addFighter(req.body);
      if (newFighter) {
        res.data = newFighter;
      }
    } catch (error) {
      const err = new Error(error);
      err.status = 400;
      next(err);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    try {
      const id = req.params.id;
      const upFighter = FighterService.updateFighter(id, req.body);
      if (upFighter) {
        res.data = upFighter;
      }
    } catch (error) {
      const err = new Error(error);
      err.status = 400;
      next(err);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      console.log("id");
      const id = req.params.id;
      const delFighter = FighterService.deleteFighter(id);
      console.log(delFighter);
      res.data = delFighter;
    } catch (error) {
      const err = new Error(error);
      err.status = 400;
      next(err);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

module.exports = router;
