const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
router.post(
  "/",
  createUserValid,
  (req, res, next) => {
    try {
      console.log(req.body);
      const user = UserService.create(req.body);
      if (user) {
        res.data = user;
        console.log(res.data);
      }
    } catch (error) {
      next(error);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  (req, res, next) => {
    try {
      const id = req.params.id;
      const upUser = UserService.updateUser(id, req.body);
      if (upUser) {
        res.data = upUser;
        console.log(res.data);
      }
    } catch (error) {
      next(error);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params.id;
      const delUser = UserService.deleteUser(id);
      if (delUser) {
        res.data = delUser;
        console.log(res.data);
      }
    } catch (error) {
      next(error);
    } finally {
      next(res.data);
    }
  },
  responseMiddleware
);

module.exports = router;
