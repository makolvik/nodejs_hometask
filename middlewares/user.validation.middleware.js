const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  if (
    req.body.password >= 3 &&
    req.body.email.includes("gmail") &&
    req.body.phoneNumber.startsWith("+380")
  ) {
    let newUser = user;
    newUser.email = req.body.email;
    newUser.firstName = req.body.firstName;
    newUser.lastName = req.body.lastName;
    newUser.phoneNumber = req.body.phoneNumber;
    newUser.password = req.body.password;
    req.body = newUser;
    next();
  } else {
    const error = new Error("Please check email or Password or Number");
    error.status = 404;
    next(error);
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  if (
    req.body.password >= 3 &&
    req.body.email.includes("gmail") &&
    req.body.phoneNumber.startsWith("+380")
  ) {
    next();
  } else {
    let error = new Error("Please check email or Password or Number");
    error.status = 404;
    next(error);
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
