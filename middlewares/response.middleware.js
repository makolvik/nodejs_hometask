const responseMiddleware = (body, req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  console.log("middle ware");
  if (body.status) {
    console.log("404");
    res
      .status(body.status || 400)
      .json({ error: true, message: body.message.toString() });
  } else {
    res.status(200).json(body);
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
