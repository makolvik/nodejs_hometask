const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation

  if (req.body.defense > 0 && req.body.defense < 11 && req.body.power < 100) {
    let newFighter = fighter;
    newFighter.name = req.body.name;
    newFighter.health = 100;
    newFighter.power = req.body.power;
    newFighter.defense = req.body.defense;
    req.body = newFighter;
    console.log("newfighter created");
    next();
  } else {
    const error = new Error(
      "Incorect data for create fighter. Please check power and defense"
    );
    error.status = 404;
    next(error);
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  if (req.body.defense > 0 && req.body.defense < 11) {
    next();
  } else {
    const error = new Error(
      "Incorect data for create fighter. Please check power and defense"
    );
    error.status = 404;
    next(error);
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
