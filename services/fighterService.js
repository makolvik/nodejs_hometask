const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getFighters() {
    const fighters = FighterRepository.getAll();
    return fighters;
  }
  addFighter(data) {
    const newFighter = FighterRepository.create(data);
    if (!newFighter) {
      throw Error("Fighter doesnt created");
    }
    return newFighter;
  }
  updateFighter(id, data) {
    const upFighter = FighterRepository.update(id, data);
    if (!upFighter) {
      throw Error("Fighter isnt updated");
    }
    return upFighter;
  }
  deleteFighter(id) {
    const delFighter = FighterRepository.delete(id);
    if (!delFighter) {
      throw Error("Fighter doesnt deleted");
    }
    return delFighter;
  }
}

module.exports = new FighterService();
