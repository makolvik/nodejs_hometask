const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  create(data) {
    const newuser = UserRepository.create(data);
    if (!newuser) {
      return null;
    }
    return newuser;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  updateUser(id, data) {
    const upUser = UserRepository.update(id, data);

    if (!upUser) {
      throw Error("User doesnt exist");
    }
    return upUser;
  }
  deleteUser(id) {
    const delUser = UserRepository.delete(id);

    if (!delUser) {
      throw Error("User doesnt exist");
    }
    return delUser;
  }
}

module.exports = new UserService();
